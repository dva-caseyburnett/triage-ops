# frozen_string_literal: true

require 'spec_helper'
require 'addressable'
require_relative '../../lib/constants/labels'
require_relative '../../lib/growth_refine_automation_helper'

RSpec.describe GrowthRefineAutomationHelper do
  let(:issue_resource) do
    Struct.new(:resource) do
      include GrowthRefineAutomationHelper
    end
  end

  describe '#refinement_completed?' do
    let(:comments_path) { 'https://test.com/comments' }
    let(:issue_path) { 'https://test.com/issue' }

    let(:resource) do
      { _links: { notes: comments_path, self: issue_path } }
    end

    subject { issue_resource.new(resource) }

    context 'when no refinement_thread is found in the issue' do
      let(:empty_comments) { [{ 'body' => 'empty' }] }

      it 'returns false' do
        stub_request(:get, comments_path).to_return(status: 200, body: empty_comments.to_json)
        expect(subject.refinement_completed?).to be false
      end
    end

    context 'when refinement_thread is found' do
      let(:emoji_path) { "#{comments_path}/#{contain_refine_comments[0]['id']}/award_emoji" }
      let(:contain_refine_comments) do
        [
          {
            'body' => '## :construction: Refinement',
            'id' => '1'
          }
        ]
      end

      let(:valid_reactions) do
        [
          {
            'name' => 'one',
            'user' => { 'id' => 1 }
          },
          {
            'name' => 'two',
            'user' => { 'id' => 11 }
          },
          {
            'name' => 'three',
            'user' => { 'id' => 111 }
          }
        ]
      end

      before do
        stub_request(:get, comments_path).to_return(status: 200, body: contain_refine_comments.to_json)
      end

      context 'without any emoji reaction' do
        it 'returns false' do
          stub_request(:get, emoji_path).to_return(status: 200, body: [].to_json)
          expect(subject.refinement_completed?).to be false
        end
      end

      context 'with enough unique valid reactions' do
        it 'returns true and triggers adding checkmark and updating weight with max weight' do
          stub_request(:get, emoji_path).to_return(status: 200, body: valid_reactions.to_json)
          stub_request(:post, emoji_path).with(body: 'name=white_check_mark').to_return(status: 201)
          stub_request(:put, issue_path).with(body: 'weight=3').to_return(status: 201)
          expect(subject.refinement_completed?).to be true
        end
      end

      context "with reactions containing 'x'" do
        it 'returns false to block refinement' do
          valid_reactions << { 'name' => 'x' }
          stub_request(:get, emoji_path).to_return(status: 200, body: valid_reactions.to_json)
          expect(subject.refinement_completed?).to be false
        end
      end
    end
  end

  describe '#qualify_for_refine?' do
    let(:growth_labels) { Addressable::URI.escape(Labels::GROWTH_TEAM_LABELS.join(',')) }
    let(:refine_list_labels) { "#{Labels::WORKFLOW_REFINEMENT_LABEL},#{growth_labels}" }
    let(:plan_list_labels) { "#{Addressable::URI.escape(Labels::WORKFLOW_PLANNING_BREAKDOWN_LABEL)},#{growth_labels}" }
    let(:gitlab_group_api_endpoint) { 'https://gitlab.com/api/v4/groups/9970/' }
    let(:board_lists) { { 'lists' => [{ 'max_issue_count' => 2, 'label' => { 'name' => Labels::WORKFLOW_REFINEMENT_LABEL } }] } }
    let(:refine_issues_stats) { { 'statistics' => { 'counts' => { 'opened' => 0 } } } }
    let(:qualified_issues) { [{ 'iid' => '1' }, { 'iid' => '2' }] }

    before do
      stub_request(:get, "#{gitlab_group_api_endpoint}boards/4152639")
        .to_return(status: 200, body: board_lists.to_json)

      stub_request(:get, "#{gitlab_group_api_endpoint}issues_statistics?labels=#{refine_list_labels}")
        .to_return(status: 200, body: refine_issues_stats.to_json)

      stub_request(:get, "#{gitlab_group_api_endpoint}issues?per_page=2&labels=#{plan_list_labels}&state=opened&order_by=relative_position&sort=asc")
        .to_return(status: 200, body: qualified_issues.to_json)
    end

    context 'when the issue is qualified' do
      subject { issue_resource.new({ 'iid' => '1' }) }

      it 'returns true' do
        expect(subject.qualify_for_refine?).to be true
      end
    end

    context 'when the issue is not qualified' do
      subject { issue_resource.new({ 'iid' => '3' }) }

      it 'returns false' do
        expect(subject.qualify_for_refine?).to be false
      end
    end
  end
end
