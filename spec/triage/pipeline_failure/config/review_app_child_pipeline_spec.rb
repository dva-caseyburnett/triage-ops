# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/review_app_child_pipeline'

RSpec.describe Triage::PipelineFailure::Config::ReviewAppChildPipeline do
  let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  let(:source_job_id) { 42 }
  let(:variables) do
    [
      { 'key' => 'REVIEW_APPS_DOMAIN', 'value' => 'foo' }
    ]
  end

  let(:builds) { [{ 'name' => 'review-deploy', 'status' => 'failed' }] }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      project_path_with_namespace: project_path_with_namespace,
      source_job_id: source_job_id,
      variables: variables,
      builds: builds)
  end

  subject { described_class.new(event) }

  describe '.match?' do
    it 'returns true' do
      expect(described_class.match?(event)).to be_truthy
    end

    context 'when project_path_with_namespace is not "gitlab-org/gitlab"' do
      let(:project_path_with_namespace) { 'foo/bar' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when source_job_id is nil' do
      let(:source_job_id) { nil }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when variables does not include REVIEW_APPS_DOMAIN' do
      let(:variables) { [] }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when builds does not include review-deploy' do
      let(:builds) { [] }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when builds does include review-deploy, but the job succeeded' do
      let(:builds) { [{ 'name' => 'review-deploy', 'status' => 'success' }] }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end
  end

  describe '#incident_project_id' do
    it 'returns expected project id' do
      expect(subject.incident_project_id).to eq('43330483')
    end
  end

  describe '#incident_template' do
    it 'returns expected template' do
      expect(subject.incident_template).to eq(described_class::INCIDENT_TEMPLATE)
    end
  end

  describe '#incident_labels' do
    it 'returns expected labels' do
      expect(subject.incident_labels).to eq(['review-apps-broken', 'Engineering Productivity', 'ep::review-apps'])
    end
  end

  describe '#slack_channel' do
    it 'returns expected channel' do
      expect(subject.slack_channel).to eq('review-apps-broken')
    end
  end
end
