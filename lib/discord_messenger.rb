# frozen_string_literal: true

require 'discordrb/webhooks'

class DiscordMessenger
  BASE_URL = "https://discord.com/api"

  def initialize(webhook_path)
    return if webhook_path.nil?

    @discord_client = Discordrb::Webhooks::Client.new(url: "#{BASE_URL}/#{webhook_path}")
  end

  def send_message(message_text)
    return if discord_client.nil?

    discord_client.execute do |builder|
      builder.content = message_text
    end
  end

  private

  attr_reader :discord_client
end
