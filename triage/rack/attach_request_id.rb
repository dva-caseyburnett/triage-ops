# frozen_string_literal: true

require 'rack'
require 'rack/requestid'

module Triage
  module Rack
    # Inspired by https://github.com/eropple/rack-ougai/blob/master/lib/rack/ougai/attach_requestid.rb
    AttachRequestId = Struct.new(:app) do
      def call(env)
        parent = env[::Rack::RACK_LOGGER]

        request_id = env[::Rack::RequestID::REQUEST_ID_KEY]

        parent.warn "No request ID in storage (is Rack::RequestID in your middleware stack?)." if request_id.nil?

        env[::Rack::RACK_LOGGER] = parent.child(request_id: request_id)
        app.call(env).tap do
          env[::Rack::RACK_LOGGER] = parent
        end
      end
    end
  end
end
