# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/pipeline_failure/config_selector'
require_relative '../../triage/pipeline_failure/config/gitaly_update_branch'
require_relative '../../triage/pipeline_failure/config/master_branch'
require_relative '../../triage/pipeline_failure/config/review_app_child_pipeline'
require_relative '../../triage/pipeline_failure/config/ruby2_branch'
require_relative '../../triage/pipeline_failure/config/security_branch'
require_relative '../../triage/pipeline_failure/config/stable_branch'
require_relative '../../triage/pipeline_failure/failed_jobs'
require_relative '../../triage/pipeline_failure/incident_creator'
require_relative '../../triage/pipeline_failure/slack_notifier'

module Triage
  class PipelineFailureManagement < Processor
    CONFIGURATION_CLASSES = [
      Triage::PipelineFailure::Config::GitalyUpdateBranch,
      Triage::PipelineFailure::Config::MasterBranch,
      Triage::PipelineFailure::Config::ReviewAppChildPipeline,
      Triage::PipelineFailure::Config::Ruby2Branch,
      Triage::PipelineFailure::Config::SecurityBranch,
      Triage::PipelineFailure::Config::StableBranch
    ].freeze

    react_to 'pipeline.failed'

    def applicable?
      event.from_gitlab_org? && config
    end

    def process
      incident = create_incident
      send_slack_notification(incident)
    end

    def documentation
      <<~TEXT
        This processor creates a pipeline failure incident and post it to Slack.
      TEXT
    end

    private

    def config
      @config ||= PipelineFailure::ConfigSelector.find_config(event, CONFIGURATION_CLASSES)
    end

    def failed_jobs
      @failed_jobs ||= Triage::PipelineFailure::FailedJobs.new(event).execute
    end

    def create_incident
      incident_creator = Triage::PipelineFailure::IncidentCreator.new(
        event: event,
        config: config,
        failed_jobs: failed_jobs)

      return unless config.create_incident?

      incident_creator.execute
    end

    def send_slack_notification(incident)
      Triage::PipelineFailure::SlackNotifier.new(
        event: event,
        config: config,
        failed_jobs: failed_jobs,
        slack_webhook_url: ENV.fetch('SLACK_WEBHOOK_URL'),
        incident: incident).execute
    end
  end
end
