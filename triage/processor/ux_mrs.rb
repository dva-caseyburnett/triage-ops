# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/processor'
require_relative '../triage/unique_comment'

require 'digest'
require 'slack-messenger'

module Triage
  class UxMrs < Processor
    UX_LABEL = 'UX'
    SLACK_CHANNEL = '#ux-mrs'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hi UX team, an MR is ready for UX review: (%<validated_group_name>s: %<mr_title>s) - %<mr_url>s.
    MESSAGE

    react_to 'merge_request.open', 'merge_request.update'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      event.from_gitlab_org? &&
        event.resource_open? &&
        !event.wip? &&
        event.team_member_author? &&
        ux_label_added? &&
        unique_comment.no_previous_comment?
    end

    def process
      send_review_request && post_ux_comment
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    def documentation
      <<~TEXT
        This processor posts reminder comment in UX merge requests to assigned reviewer suggested by Reviewer Roulette,
        and also pings the UX team slack channel about the merge request.
      TEXT
    end

    private

    attr_reader :messenger

    def ux_label_added?
      event.added_label_names.include?(UX_LABEL)
    end

    def send_review_request
      slack_message = format(SLACK_MESSAGE_TEMPLATE, mr_url: event.url, mr_title: event.title, validated_group_name: validated_group_name)
      messenger.ping(slack_message)

      true
    end

    def post_ux_comment
      add_comment(message.strip, append_source_link: true)
    end

    def message
      comment = <<~MESSAGE
        Please wait for Reviewer Roulette to suggest a designer for UX review, and then assign them as Reviewer. This helps evenly distribute reviews across UX.
      MESSAGE

      unique_comment.wrap(comment)
    end

    def slack_messenger
      Slack::Messenger.new(ENV.fetch('SLACK_WEBHOOK_URL', nil), slack_options)
    end

    def label_names
      @label_names ||= event.label_names
    end

    def unscope_label(label)
      label.split('::').last
    end

    # @return [String,nil] the full name of the group label, excluding unsupported labels
    # @see #validated_group_name to return the label name (without the scope)
    def group_label
      label_names.find do |label_name|
        label_name.start_with?('group::')
      end
    end

    def validated_group_name
      return 'No group label' unless group_label

      parse_group_name_from_label(group_label)
    end

    def parse_group_name_from_label(group_label)
      raw_group_name = unscope_label(group_label)

      group_name =
        case raw_group_name
        when 'distribution'
          # Labels such as Distribution:Build and Distribution:Deploy can be used to identify the specific group
          label_names.find { |label| label.start_with?('Distribution:') } || 'distribution_build'
        when 'gitaly'
          # Labels such as gitaly::cluster and gitaly::git can be used to identify the specific group
          label_names.find { |label| label.start_with?('gitaly::') } || 'gitaly_cluster'
        else
          raw_group_name
        end

      group_name.titleize
    end
  end
end
