# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative 'triage_incident'

module Triage
  module PipelineFailure
    class IncidentCreator
      def initialize(event:, config:, failed_jobs:)
        @event = event
        @config = config
        @failed_jobs = failed_jobs
      end

      def execute
        payload = {
          issue_type: 'incident',
          description: format(config.incident_template, template_variables),
          labels: config.incident_labels,
          **config.incident_extra_attrs
        }

        Triage.api_client.create_issue(config.incident_project_id, title, payload).tap do |incident|
          Triage.api_client.post(
            "/projects/#{config.incident_project_id}/issues/#{incident.iid}/discussions",
            body: { body: root_cause_analysis_body })
          Triage.api_client.post(
            "/projects/#{config.incident_project_id}/issues/#{incident.iid}/discussions",
            body: { body: "## Investigation Steps" })
        end
      end

      private

      attr_reader :event, :config, :failed_jobs

      def now
        @now ||= Time.now.utc
      end

      def title
        @title ||= begin
          full_title = "#{now.strftime('%A %F %R UTC')} - `#{event.project_path_with_namespace}` " \
                       "broken `#{event.ref}` with #{failed_jobs.map(&:name).join(', ')}"

          if full_title.size >= 255
            "#{full_title[...252]}..." # max title length is 255, and we add an elipsis
          else
            full_title
          end
        end
      end

      def template_variables
        {
          project_link: project_link,
          pipeline_link: pipeline_link,
          branch_link: branch_link,
          commit_link: commit_link,
          triggered_by_link: triggered_by_link,
          source: source,
          pipeline_duration: pipeline_duration,
          failed_jobs_count: failed_jobs.size,
          failed_jobs_list: failed_jobs_list,
          merge_request_link: merge_request_link
        }
      end

      def project_link
        "[#{event.project_path_with_namespace}](#{event.project_web_url})"
      end

      def pipeline_link
        "[##{event.id}](#{event.web_url})"
      end

      def branch_link
        "[`#{event.ref}`](#{event.project_web_url}/-/commits/#{event.ref})"
      end

      def commit_link
        "[#{event.commit_header}](#{event.project_web_url}/-/commit/#{event.sha})"
      end

      def triggered_by_link
        # Recreate the server URL from event.project_web_url...
        "[#{event.event_actor.name}](#{event.project_web_url.delete_suffix(event.project_path_with_namespace)}#{event.event_actor.username})"
      end

      def source
        "`#{event.source}`"
      end

      def pipeline_duration
        ((now - event.created_at) / 60.to_f).round(2)
      end

      def failed_jobs_list
        failed_jobs.map { |job| "- [#{job.name}](#{job.web_url})" }.join("\n")
      end

      def merge_request_link
        return 'N/A' unless event.merge_request

        "[#{event.merge_request.title}](#{event.merge_request.web_url})"
      end

      def root_cause_analysis_body
        body = '## Root Cause Analysis'
        body += "\n\n#{TriageIncident.new(event, failed_jobs).root_cause_analysis_comment}" if event.ref == 'master'

        body
      end
    end
  end
end
